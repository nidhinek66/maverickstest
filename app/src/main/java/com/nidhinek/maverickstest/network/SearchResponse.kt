package com.nidhinek.maverickstest.network

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("Search") val movieList: List<Movie>,
    @SerializedName("totalResults") val totalResults: Int,
    @SerializedName("Response") val isSuccessResponse: Boolean
)
