package com.nidhinek.maverickstest.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nidhinek.maverickstest.network.Movie
import com.nidhinek.maverickstest.network.MovieDetail
import com.nidhinek.maverickstest.repo.MovieRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val repository: MovieRepository) : ViewModel() {
    val errorMessage = MutableLiveData<String>()
    val movieList = MutableLiveData<List<Movie>>()
    private val selectedMovie = MutableLiveData<Movie>()
    val movieDetail = MutableLiveData<MovieDetail>()
    val loading = MutableLiveData<Boolean>()

    fun getAllMovies(title: String?, type: String?) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.getAllMovies(title, type)
            withContext(Dispatchers.Main) {
                if (response.isSuccessResponse) {
                    movieList.postValue(response.movieList)
                    loading.value = false
                } else {
                    onError("Something went wrong")
                }
            }
        }
    }

    fun getMovieDetail() {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.getMovieDetails(selectedMovie.value?.imdbID)
            withContext(Dispatchers.Main) {
                movieDetail.postValue(response)
                loading.value = false
            }
        }
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    fun setSelectedMovie(movie: Movie) {
        selectedMovie.value = movie
    }
}
