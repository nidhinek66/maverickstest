package com.nidhinek.maverickstest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.nidhinek.maverickstest.R
import com.nidhinek.maverickstest.network.ApiService
import com.nidhinek.maverickstest.network.MovieDetail
import com.nidhinek.maverickstest.repo.MovieRepository

class MovieDetailFragment : Fragment() {
    private lateinit var mainViewModel: MainViewModel
    private lateinit var imageView: ImageView
    private lateinit var titleTextView: TextView
    private lateinit var yearTextView: TextView
    private lateinit var directorTextView: TextView
    private lateinit var writerTextView: TextView
    private lateinit var actorTextView: TextView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_movie_detail, container, false)
        imageView = view.findViewById(R.id.imageView)
        titleTextView = view.findViewById(R.id.title)
        yearTextView = view.findViewById(R.id.year)
        directorTextView = view.findViewById(R.id.director)
        writerTextView = view.findViewById(R.id.writer)
        actorTextView = view.findViewById(R.id.actor)

        val apiService = ApiService.getInstance()
        val mainRepository = MovieRepository(apiService)

        activity?.let {
            mainViewModel = ViewModelProvider(
                it,
                CustomViewModelFactory(mainRepository)
            ).get(MainViewModel::class.java)
        }
        mainViewModel.movieDetail.observe(this, {
            setData(it)
        })
        mainViewModel.getMovieDetail()
        return view
    }

    private fun setData(movieDetail: MovieDetail?) {
        Glide.with(imageView.context)
            .load(movieDetail?.poster) // image url
            .centerCrop()
            .into(imageView)
        movieDetail?.let {
            titleTextView.text = it.title
            yearTextView.text = it.year
            actorTextView.text = getString(R.string.actor).plus(it.actors)
            directorTextView.text = getString(R.string.director).plus(it.director)
            writerTextView.text = getString(R.string.writer).plus(it.writer)
        }
    }
}
