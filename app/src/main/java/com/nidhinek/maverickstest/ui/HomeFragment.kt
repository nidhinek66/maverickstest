package com.nidhinek.maverickstest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nidhinek.maverickstest.MainActivity
import com.nidhinek.maverickstest.R
import com.nidhinek.maverickstest.network.ApiService
import com.nidhinek.maverickstest.network.Movie
import com.nidhinek.maverickstest.repo.MovieRepository


class HomeFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var searchView: SearchView
    private var spanCount: Int = 2
    private var movieList: ArrayList<Movie> = arrayListOf()
    private val movieAdapter = MovieAdapter()
    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        searchView = view.findViewById(R.id.searchView)
        initRecyclerView()
        handleSearch()
        val apiService = ApiService.getInstance()
        val mainRepository = MovieRepository(apiService)
        activity?.let {
            mainViewModel = ViewModelProvider(
                it,
                CustomViewModelFactory(mainRepository)
            ).get(MainViewModel::class.java)
        }

        mainViewModel.movieList.observe(this, {
            movieAdapter.update(it)
        })

        mainViewModel.errorMessage.observe(this, {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })

        mainViewModel.loading.observe(this, Observer {
            //loader display handle here
        })
        getMoviesWithDefaultParams()
        return view
    }

    private fun handleSearch() {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    mainViewModel.getAllMovies(it, "movie")
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
        searchView.setOnCloseListener {
            getMoviesWithDefaultParams()
            true
        }
    }

    private fun getMoviesWithDefaultParams() {
        mainViewModel.getAllMovies("Marvel", "movie")
    }

    private fun initRecyclerView() {
        val layoutManager = GridLayoutManager(context, spanCount)
        recyclerView.layoutManager = layoutManager
        setData()
    }

    private fun setData() {
        movieAdapter.update(movieList)
        recyclerView.adapter = movieAdapter
        recyclerView.addItemDecoration(GridSpaceItemDecorator(resources.getDimension(R.dimen.sixteendp)))
        movieAdapter.onItemClick = {
            mainViewModel.setSelectedMovie(it)
            (activity as? MainActivity)?.addFragmentToActivity(MovieDetailFragment(), true)
        }
    }
}
