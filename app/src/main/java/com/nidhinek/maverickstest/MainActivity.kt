package com.nidhinek.maverickstest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.nidhinek.maverickstest.ui.HomeFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        addFragmentToActivity(HomeFragment())
    }

    fun addFragmentToActivity(fragment: Fragment?, addToBackStack: Boolean = false) {
        if (fragment == null) return
        val fm = supportFragmentManager
        val tr = fm.beginTransaction()
        tr.add(R.id.mainContainer, fragment)
        if (addToBackStack) tr.addToBackStack(null)
        tr.commit()
    }
}
