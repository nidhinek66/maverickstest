package com.nidhinek.maverickstest.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    companion object {
        var apiService: ApiService? = null
        fun getInstance(): ApiService {
            if (apiService == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://www.omdbapi.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
                apiService = retrofit.create(ApiService::class.java)
            }
            return apiService!!
        }

    }

    @GET("/?apikey=b9bd48a6")
    suspend fun searchMovies(
        @Query("s") title: String?,
        @Query("type") type: String?
    ): SearchResponse

    @GET("/?apikey=b9bd48a6")
    suspend fun getMovieInfo(
        @Query("i") id: String?,
    ): MovieDetail
}
