package com.nidhinek.maverickstest.ui

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class GridSpaceItemDecorator(space: Float) : RecyclerView.ItemDecoration() {
    // amount to add to padding
    private var space: Int = 0

    init {
        this.space = space.toInt()
    }

    /**
     * Applies padding to all sides of the [Rect], which is the container for the view
     */
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(space, space, space, space)
    }
}
