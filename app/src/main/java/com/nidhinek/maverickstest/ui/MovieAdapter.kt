package com.nidhinek.maverickstest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nidhinek.maverickstest.R
import com.nidhinek.maverickstest.network.Movie


class MovieAdapter : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    private var movieList: ArrayList<Movie> = arrayListOf()
    var onItemClick: ((Movie) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.image_item_view, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(movieList[position])
    }

    fun update(newMovies: List<Movie>) {
        movieList.clear()
        movieList.addAll(newMovies)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = movieList.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageView: ImageView = itemView.findViewById(R.id.imageView)
        private val textView: TextView = itemView.findViewById(R.id.textView)

        init {
            imageView.setOnClickListener {
                onItemClick?.invoke(movieList.get(adapterPosition))
            }
        }

        fun bindData(movie: Movie) {
            Glide.with(imageView.context)
                .load(movie.poster) // image url
                .centerCrop()
                .into(imageView);
            textView.text = movie.title
        }
    }
}
