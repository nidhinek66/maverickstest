package com.nidhinek.maverickstest.repo

import com.nidhinek.maverickstest.network.ApiService

class MovieRepository constructor(private val apiService: ApiService) {
    suspend fun getAllMovies(title: String?, type: String?) = apiService.searchMovies(title, type)
    suspend fun getMovieDetails(id: String?) = apiService.getMovieInfo(id)
}
